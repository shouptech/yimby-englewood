---
title: CodeNext Passed!
date: 2023-09-26T15:00:00-06:00
featured_image: img/cover.jpg
---

With everything that's going on in the city, I feel it's important to call out that CodeNext passed the second reading last night! You can view the vote and brief discussion on the [city's youtube](https://www.youtube.com/live/7YHlu3ECD1A?si=nR2ftZvunQOieLIn&t=11456):

Off the top of my head, here's a list of some of the major things CodeNext accomplishes:

* ADUs in all residential zoning districts. R-1 zones are allowed one ADU on the property, and R-2-B can have up to 3 ADUs (1 external and 2 internal). This was a huge point of contention amongst members of council and the public. ADUs are allowed with no additional off street parking, and no owner occupancy requirement. I consider both of those a win, as it lowers the barrier of entry to building ADUs.
* Corner lots may be split. This one is a great win. It allows certain corner lots to be split into two lots, a larger front lot, and a smaller rear lot. This is a pattern we already see in the city, but at some point became illegal. The sizes of the lots and how they can be split varies by zoning district. For example, a 6,000 sq ft lot in R-1-C (which is a standard lot size for that district) can be split into a 4,000 sq ft lot and a 2,000 sq ft lot.
* Courtyard lots! I love this, certain lots can be split into multiple, smaller lots if they center around a courtyard. Minimum lot sizes vary by district, but in R-1-C, you could have as small as a 2,000 sq ft lot.
* Bulk planes were reduced. One of the more recent updates to our zoning code increased the size of the bulk plane, which is resulting in larger more "boxy" style buildings. Reducing the bulk plane forces buildings to have a more "traditional" tapered roof design, fitting in better with the existing aesthetics of Englewood.
* The code regarding unrelated persons was changed from 2 unrelated adults, to 4 unrelated adults and their dependents. This is important here, as it allows more people to have roommates and share a house, reducing the burden of housing payments. I was surprised to hear that the previous code only allowed 2 unrelated adults to live with each other. I have friends that were unknowingly violating this code. It's not an uncommon scenario for a house to have more than 2 roommates living together.
* Removed around 40 pages of legalese through simplifying our code. It's still large, but this certainly helps make things simpler.

There's a ton more to it, and this post would be quite long if I wrote everything! These are just some of the larger wins to having this pass. Unless there is some sort of intervention from a citizen petition or a lawsuit, this will go into effect on October 26, 2023.

The final draft of the code can be [found on the CodeNext website](https://www.engaged.englewoodco.gov/codenext).
