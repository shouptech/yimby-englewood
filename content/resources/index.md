---
title: "Resources"
menu:
  main:
    weight: 3
---

This page is an attempt to compile a list of resources for those who wish to learn more about what we're talking about here.

## Stay Informed!

Make sure you are signed up to get emails from the city.

- [Sign up for text & email notifications](https://www.englewoodco.gov/our-city/text-email-notifications) from the city.
- Want to know when meetings take place? [Get email notifications of future meetings](https://englewoodgov.civicweb.net/Portal/Subscribe.aspx).


## City of Englewood

- [Contact the city council](https://www.englewoodco.gov/government/city-council)
- [Attend council meetings](https://englewoodgov.civicweb.net/portal/)
- [Read more about CodeNext](https://www.engaged.englewoodco.gov/codenext)
- [View the city's zoning map](https://www.englewoodco.gov/government/city-departments/community-development/zoning/zoning-map)

## The State

Housing is not just a local issue, but a state-wide issue! Our legislators are considering how to help the housing crisis. Below are some articles discussing their plans:

- The Colorado Sun - [Colorado Democrats are turning 2023 into the year of housing. But should the state wade into local land decisions?](https://coloradosun.com/2023/02/12/colorado-housing-local-control-density/)
- Colorado Politics - [Colorado legislators introduce first bills to spend $850 million for housing, behavioral health](https://www.coloradopolitics.com/legislature/colorado-legislators-introduce-first-bills-to-spend-850-million-for-housing-behavioral-health/article_b5120a42-9f1d-11ec-99d6-5bff70ff3698.html)

## External Articles

These articles are intended to provide sources and references for discussing the benefits of density.

- The Guardian - [Denser cities could be a climate boon – but nimbyism stands in the way](https://www.theguardian.com/us-news/2021/aug/22/cities-climate-change-dense-sprawl-yimby-nimby)
- [What are the characteristics of Missing Middle Housing?](https://missingmiddlehousing.com/about/characteristics)
- EPA - [Protecting Water Resources with Higher-Density Development](https://www.epa.gov/smartgrowth/protecting-water-resources-higher-density-development)
- Vox - [Making cities more dense always sparks resistance. Here’s how to overcome it.](https://www.vox.com/2017/6/20/15815490/toderian-nimbys)
- National Association of Realtors - [Growing Up and Not Out: The Fiscal Benefits of Higher Density Development ](https://www.nar.realtor/articles/growing-up-and-not-out-the-fiscal-benefits-of-higher-density-development)
- Brookings Institution - [“Gentle” density can save our neighborhoods](https://www.brookings.edu/research/gentle-density-can-save-our-neighborhoods/)
- Strong Towns - [What Does Incrementalism Actually Mean?](https://www.strongtowns.org/journal/2018/12/12/what-does-incrementalism)
- NPR - [The U.S. needs more affordable housing – where to put it is a bigger battle](https://www.npr.org/2023/02/11/1155094278/states-cities-end-single-family-zoning-housing-affordable)

## The Opposition

We at YIMBY Englewood encourage you to educate yourself about the opposition. Through understanding and listening, we can come up pragmatic solutions to the city's housing problems.

- [Citizens of Englewood for Responsible Zoning](https://stopenglewoodopoly.com/)
