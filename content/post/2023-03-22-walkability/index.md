---
title: Urban Density and Walkability
date: 2023-03-22T19:43:00-06:00
featured_image: 'images/front.jpg'
---

I, like many geriatric millennials, really enjoy listening to podcasts.  One of my favorites, [America Dissected](https://crooked.com/podcast-series/america-dissected/), will continually talk about how unhealthy our cities and suburban areas have become.  By spreading out all of our recreation, jobs, shops, schools, and homes we have become car dependent by nature.  Though the complexities of a car dependent culture are not the focus of YIMBY Englewood, it is important to consider this as we look toward building a more sustainable community.  I recently came across a [Washington Post article](https://www.washingtonpost.com/news/where-we-live/wp/2017/10/09/walkable-neighborhoods-provide-health-environmental-and-financial-benefits/) (possible pay wall) that discusses the benefit of walkable neighborhoods.  The benefit of walkability and increased the desirability of these neighborhoods, which in turn, has made them unaffordable. In Englewood some of the most expensive houses that are not on the largest lots are actually the walkable homes, the ones north of US 285 and within a few blocks of Broadway, Downing, and Englewood City Center.  These homes are walkable to some of the best places in Englewood; schools, public transit, the hospital, downtown, and the Library.  We can help to build this type of desirability into the rest of Englewood by hopefully filling in the “missing middle”.

Missing middle housing, by definition, is affordable.  These units are smaller, and therefore you end up purchasing less house.  These are the starter homes that most people purchase for the first time.  Because of restrictive zoning many of these types of homes have disappeared and have been replaced by luxury units.  The picture here shows the very little difference in the front elevation of a multi-unit home versus a single family home, we can now put four families on the same lot instead of one.

![Floor Plan](images/floor-plan.jpg)
![Front of House](images/front.jpg)

Images are from [this Denver Urbanism Article](https://denverurbanism.com/2019/02/denvers-residential-neighborhoods-who-are-they-for.html)

According to the Washington Post article linked above:
> Walkable communities support the local business environment and increase its appeal to tourists. When shops are clustered together, foot traffic visitors are encouraged to visit multiple neighboring stores and spend money. In this sense, success breeds success. When local entrepreneurs see small shops doing well, they are often inspired to open their own.

By building more walkable housing units we will bring more foot traffic, more foot traffic means more small businesses.  More small businesses means a more sustainable Englewood.
