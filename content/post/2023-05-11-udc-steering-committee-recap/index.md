---
title: May 8th Council Study Session Recap
date: 2023-05-16T10:00:00-06:00
featured_image: img/cover.jpg
---

Here is my recap on the housing portion of the [May 8th city council study session](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Org=Cal&Id=3672).  I encourage you to watch the whole thing if you are interested in the nitty gritty of discussion.

The housing portion of the study session started with a UDC Steering committee update and how the process has been going.  This was insightful because it got into how a committee like this functions.  Much of the discussion circled around the goals of the committee and how the discussions evolved over time.  What worked with this committee was the relationship with planning and zoning, meeting timing, and the outside groups to help with discussion.  What could be improved upon would be more frequent meetings with council and other relevant committees so that everyone was getting the same information at the same time, more time to digest and discuss the significant amount of information that was being presented, more diagrams and pictures that help explain the different points of discussion, and a possible use case of a task force to get more feedback from the community.  This discussion also centered around in person versus online meeting and the pros/cons there, having more workshops and doing this type of updating more often so that the change is incremental rather than sudden.

The next housing discussion centered around CodeNext.  It is almost ready for adoption!  With the draft nearing completion there are some updates that they are looking at, please watch the video for the discussion from each council member.

### R1–Stay single family detached

- Allow for smaller lot sizes and ADUs
- This discussion is ongoing, watch the video for insights from each member of council.

### R2

- Allow for smaller lots
- 2-4 unit buildings
- Limited number in R2A, fully allowed in R2B
- 5-8 unit small apartments allowed in R2B in a limited capacity

### MUR3A-C

- Mixed use
- Smaller lots
- Possible larger apartments

They then went into discussion on affordability incentives in order to make affordable housing (instead of mandating affordable housing).  Height bonuses and parking reduction for so many affordable units and the like.

The discussion then went into a deep dive on ADUs and the possible impact on the R1 districts.  They discussed how many can be on each lot (depending on size), attached versus detached, the size limits, clear definition, parking, and green space.  Parking was punted to planning and zoning, while trees and green space was punted to sustainability.  The discussion then went into the bulk plane which was then sent back to UDC.

All of that was fascinating to listen to and I encourage everyone, who has time, to listen to it and hear what your council members think of these proposals.
Lastly, the council discussed the formation of an affordable housing task force.  They went into the purpose and mission of the task force (Mayor Pro-Tem Ward wanted to make sure it is people focused).  The goals of this task force would be to develop plans and strategies as well as provide information on affordable housing.  Council also discussed the current stock of affordable housing, what the city itself can do and the AMI (area mean income) and what we can do, as a city, to make sure that number reflects the people who live in Englewood.  They then discussed who should be put on this task force and how to include major employers and how to get the best, most relevant information for this task force.
