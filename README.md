# YIMBY Englewood

This is the Hugo-based website for [YIMBY Englewood](https://yimbyenglewood.org).

NOTE: YIMBY Englewood is no more. Please visit [Vibrant Englewood](https://vibrantenglewood.org/)

## License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-sa/4.0/). Please share this content and do good with it!
