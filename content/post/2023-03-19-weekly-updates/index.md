---
title: March 19, 2023 Weekly Updates
date: 2023-03-19T12:55:00-06:00
featured_image: ''
---

The below information was sent in an e-mail to those who have signed up. If you'd like to receive these e-mails, please click the [about](/about) link, and fill out the contact form!

In addition to below, we now have a Facebook page! You can find us on Facebook under [YIMBY Englewood](https://www.facebook.com/yimbyenglewood). Please follow and share with anyone you think that might be interested!

## What's Happening - March 19, 2023

Last week:

- On Monday, 3/13, the city held a study session to discuss the CodeNext changes. You can watch the meeting on [YouTube](https://www.youtube.com/watch?v=-TNpl6BxInY). The discussion is at the beginning of the meeting, and took 2 hours and 45 minutes! It's a long watch but very informative. Of the changes being discusses, Mayor Pro-Tem Ward had a suggestion of capping the number of new units allowed in R-1 until we get a better idea of the impact.
- On Thursday, 3/16, Mayor Sierra held a town hall meeting to discuss attainable housing. Unfortunately, we weren't able to make it to that meeting. I hope some of you were able to and found it enlightening!

Coming up:

- Tomorrow, Monday 3/20, is a regular city council meeting at 6pm. A number of folks have already signed up to speak about CodeNext and housing issues (including myself). The agenda can be found on the [meeting portal](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Org=Cal&Id=3644). I would greatly appreciate it if more of us spoke in favor of these changes as you can be sure there are plenty of folks there to speak against it. If you have the time and are willing, you can e-mail the city clerk (CityClerk@englewoodco.gov) to sign up. In addition to public comment, the other notable item on the agenda is the vote on the Sam's Automotive redevelopment. It's sure to be a riveting session.
- On Tuesday, 3/21, the "Unified Development Code Steering Committee" will be meeting and discussing CodeNext at 7pm. This meeting is held [online via zoom](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Org=Cal&Id=3832). The public will be given a chance to speak here as well, and I'm intending to join the zoom session and give me 3 minutes in support of these changes.
- On Saturday, 3/25 @ 1pm, Mayor Pro Tem Ward will hold a town hall meeting at the Englewood civic center. There is no specific topic he's discussing, but you can be certain that housing is going to be a very hot topic here.

BTW, all these meetings and more are easily discoverable via the city's [online meetings portal](https://englewoodgov.civicweb.net/Portal/Default.aspx).
