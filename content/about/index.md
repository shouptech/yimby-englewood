---
title: "About"
menu:
  main:
    weight: 4
---

## Mission

We at YIMBY Englewood are a group of Englewood, CO residents. YIMBY stands for Yes In My
Backyard. We advocate for changing Englewood's development code to allow for incremental
development of our city. We do this because we believe that density is good and
affordable living should be something everyone has access to.

## Mailing List

Sign up for our weekly e-mails using this form: [Sign up for our email list!](/mailinglist)

## Contact Us

Use the below form to send us an email

{{< form-contact action="https://formspree.io/f/xpzelobn" >}}
