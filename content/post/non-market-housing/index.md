---
title: "Non-Market Rate Housing"
description: "A non-capitalist solution to housing attainability"
date: 2023-03-15
featured_image: 'images/cover.jpg'
---

When it comes to adding housing, North American cities tend to rely very heavily on the capitalist market to do so. While this has worked for a very long time, it's not the only, or even optimal solution. The city of Englewood is no stranger to this either. While we have some non-market rate housing options, the large majority of our housing stock is market-rate housing.

Uytae Lee of the Youtube Channel [AboutHere](https://www.youtube.com/@AboutHere) put out a [video recently](https://www.youtube.com/watch?v=sKudSeqHSJk) on how non-market rate housing has the potential to help our cities. While he mostly discusses how it is applied in Vancouver, we can take some of the lessons learned in other cities and incorporate non-market rate housing in with our housing plans.

That is _exactly_ what CodeNext plans to do! Our mayor, Othoniel Sierra, discusses this on [page 4 of the Englewood Magazine](https://www.englewoodco.gov/home/showpublisheddocument/31175/638108434480730000):

> City council is considering allowing duplexes, triplexes or quadplexes to be built-in single-family areas, under the condition that one of the homes is priced at 30 to 80% AMI (Area Median Income, or for households who make between $24,560 to $89,400/year). This would allow the supply of market rate homes to increase but also provide the “carrot” needed to allow an increase in affordable housing we currently don’t possess.

The video from AboutHere is below, but the gist is a healthy mix of free-market housing as well as non-market rate housing has shown to help reduce the over all costs of housing in other cities, and I'm glad to hear that Englewood is considering this as part of the solution.

{{< youtube sKudSeqHSJk >}}
