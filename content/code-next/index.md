---
title: "CodeNext"
menu:
  main:
    weight: 1
---

CodeNext is the name of the project to update the city of Englewood's Unified Development Code. The Unified Development Code (UDC) is the code of laws that describe the city's zoning regulations and zoning districts. These laws basically dictate what can be built and where it can be built.

We encourage everyone to get educated and learn what changes are being proposed in CodeNext. Learn more about the changes by visiting the [CodeNext Website](https://www.engaged.englewoodco.gov/codenext).

Curious what your zoning district is? See the the [Interactive Zoning Map](https://www.englewoodco.gov/government/city-departments/community-development/zoning/zoning-map)

## Sensible Solutions

Some changes that are currently being considered:

- Allow ADUs to be built in all areas of the city currently zoned for single-family residential. Currently ADUs are only allowed in one specific zoning district, R-1C.
- In zoning districts that currently allow low density single-family and multi-family homes, allow more building types with a reduced emphasis on density and an increased attention to building scale and mass.
- In single-family home zoning districts, introduce the possibility of allowing two to four unit housing in certain conditions, but require that at least one unit be considered affordable to a renter or buyer earning a certain percentage of the Englewood median income.
- Reducing minimum parking requirements, emphasizing shared parking strategies and more flexibility in parking regulations.

These changes are all sensible solutions and inline with the idea of incremental development. They will not change our city overnight, nor will they immediately solve our housing crisis, but these changes are definitely a step in the right direction.
