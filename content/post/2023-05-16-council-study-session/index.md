---
title: UDC Steering Committee Recap 4/18 and 5/2
date: 2023-05-11T10:00:00-06:00
featured_image: img/cover.jpg
---

The [April 18th UDC Steering Committee meeting](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Id=3834) consisted of two parts.  The first focused on a housing needs assessment followed up with the continued discussion on the bulk plane.

Starting with the housing needs assessment, they identified the fact that Englewood does not have any middle housing.  Our housing prices have tripled since 2000 and have seen huge gaps in affordability.  There are four things to consider in order to grow sustainably: funding, building, preserving, and assisting.  Projects that have an affordable housing element (as well as the community has a whole) should consider those four pillars.  This can be thought of as diverse housing options, additional affordable rentals, more reasonably priced starter homes, and resources to stabilize households.  The discussion then went into a feasibility analysis that will build affordability as well as be attractive to developers.  Some of the proposed options include; income restriction on some for sale units, building on smaller lots, giving bonus floors, and lowering parking requirements.  Parking was brought up as a major issue, especially with larger developments.  Questions remain; how can parking not be an issue in surrounding areas and how can these developments not be disruptive to neighbors while helping with affordability and attainability.

This was then followed up with a discussion of the bulk plane and how that can be used to prevent disruption in a neighborhood.  This was a continuation from the last meeting with better definitions.  [The May 2nd meeting seemed](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Id=3857) to have continued this discussion but there was no video to watch.  The attached presentation gave better visuals for the conversation on April 18th, with visuals on the encroachment exceptions that have been discussed in previous meetings.
