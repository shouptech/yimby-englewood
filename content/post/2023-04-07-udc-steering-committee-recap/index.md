---
title: UDC Steering Committee Meeting Recap
date: 2023-04-07T12:00:00-06:00
featured_image: img/cover.jpg
---

In the pursuit to become more familiar with how CodeNext is shaping up I spent some time listening to the last two Unified Development Code Steering Committee meetings.  (you can find these links [here](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Org=Cal&Id=3832) and [here](https://englewoodgov.civicweb.net/Portal/MeetingInformation.aspx?Org=Cal&Id=3839)). One thing I can say for sure is that I learned a lot!  Since current discussions on actual zoning changes are on pause I’m going to focus on the most recent takeaways on other aspects of CodeNext that can have a huge impact on housing types and possible affordability.  No matter what happens with zoning at the state level local municipalities will control the design requirements of all new development.  I’m hoping to continue to summarize these meetings throughout the CodeNext project so that these changes are less scary to people.

## Some of the takeaways from the last two meetings

- ADU’s–the site should appear as is, from the front
  - No need for extra parking as these are small outbuildings that
  - There was also some discussion on adding a main unit to a lot that has a large front yard
- Help encourage more development in R2A and R2B zones because they are already zoned for more units and it is an easy place to add density
- Lots of discussion on the bulk plane
  - Changing the bulk plane to 15 ft instead of 17 ft as a compromise to allow for less impact on solar
  - Using dormers to help add space within the 15 ft bulk plane
- Setback variance and how those can be used to add density but not change neighborhood character too much
  - This also included discussion with modular homes as they are usually longer so building these can be challenging with setbacks sometimes
- Changing the manufacture home rules around the size of the area of the mobile home parks so that the space can be better used
  - This also includes possibly adding tiny homes

The next steering committee meeting is April 18th!
