---
title: "Incremental Development"
date: 2023-03-14
featured_image: 'images/cover.jpg'
---

We at YIMBY Englewood consider incremental development to be a key value.

Current Englewood zoning code does not allow incremental development in our city. The
vast majority of our lots are zoned for single family residential. This forces new
projects to add housing units into small parts of the city. This creates incentive for
large projects that can dramatically change the city in a very short period of time.
Another term for this is "sudden intensification."

With incremental development, the city is allowed to adapt and change as needed.
Individual property owners are allowed to add housing units to their property when it
makes sense to do so.

Incremental development is a key principle, and part of the mission of
[Strong Towns](https://www.strongtowns.org/). For an excellent primer on incremental development, please see this article from
Strong Towns: [What Does Incrementalism Actually Mean?](https://www.strongtowns.org/journal/2018/12/12/what-does-incrementalism)

![Incremental Development vs Sudden Development](images/art.jpg)

Images are [shared by Strong Towns](https://www.strongtowns.org/journal/2018/12/12/what-does-incrementalism)
using a CC BY-SA 3.0 license.
