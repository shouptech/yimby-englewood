---
title: A Form Letter for your Representatives
date: 2023-04-01T09:00:00-06:00
featured_image: ''
---

Below is a form letter you can use to e-mail our representatives, Representative Froelich and Senator Jeff Bridges:

> Dear Representative Froelich and Senator Bridges
>
> My name is _____________ and I am a constituent from Englewood, Co.  I am writing to you today to express my support for the new land use proposals unveiled by Governor Polis on March 21st.  Though I think that land use should be a bottom up approach as opposed to the top down approach of these proposals, a statewide approach will help alleviate some of the pressure front range communities face.  Here in Englewood we are currently debating the best way to grow with our zoning code updates that we call CodeNext.  This update will allow Englewood to grow in a sustainable way.  The aims of CodeNext are in line with the land use proposals that Governor Polis has presented and I believe that our state representatives should bring the support of these changes to the state level.
>
> Thank you for your time,
>
> ______________'
