---
title: "Incremental Development in Established Neighborhoods"
description: "How zoning broke our neighborhoods and how it can help fix them"
date: 2023-03-18
featured_image: 'images/cover.jpg'
---

It is always challenging to articulate urban development in a way that doesn’t cause feelings of fear.  This is because for the last 100 years America has been living under what we now consider “conventional zoning”.  This type of zoning was developed after large skyscrapers were being built in New York City without the consideration of the neighborhood surrounding them.  The basic premise of building larger than what is around is jarring and can cause harm in the neighborhood.

To prevent this type of overwrought development, many municipalities rezoned areas as industrial, commercial, and residential, with many sub-zoning categories under each of those broad headings.  When it comes to residential zoning we accidentally created a problem, our neighborhoods were not walkable, nor all that desirable to actually live in. Many factors contributed to this but chief among them were; distance to commercial centers, lot size, building setbacks, and parking minimums[^1].  It is hard to live in an area when you have to drive out of it to enjoy any amenities.

How do we get out of the mess of conventional zoning?  Let’s look towards cultures in “developing” and indigenous areas.  Many of these communities consider their homes and neighborhoods living in the sense that they should grow and change.  The value in this worldview is that neighborhoods are allowed to incrementally develop.  What this means is instead of a 4-story apartment complex going in on what used to be three single family homes, a family puts in a granny flat into the back yard to accommodate an aging parent.  We might also see a derelict house be torn down and a set of townhomes put in its stead.

![Incremental Development](images/incremental-development.jpg "Incremental Development")

Incremental home changes over time.  Grey is the original structure, red are the additional structures[^2].

*Note: The second link below is from a scholarly article in JSTOR. Access can be gained by registering for a free account, which gives you 100 free articles per month.*

[^1]: [A Brief History of American Zoning](https://www.billingsmt.gov/DocumentCenter/View/34836)
[^2]: [The Value of Incremental Development and Design in Affordable Housing](https://www.jstor.org/stable/26326881)
