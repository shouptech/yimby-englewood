---
title: Dense Housing is Affordable Housing
date: 2023-03-14
featured_image: 'images/cover.jpg'
---

Housing is quickly becoming unaffordable for many families. Combining the rapid rise of housing prices with today's mortgage rates (6.75%![^1]), it's no wonder many families are struggling to pay their rents and mortgages, let alone buy a home in this market.

By gradually adding more homes in single-family neighborhoods, it is possible to improve home affordability. A study by the Brookings Institution[^2] shows that where land is expensive, building more homes per parcel increases affordability.

Englewood is land-locked and has nowhere to grow its borders. The price of land within the city is only going to increase. If we don't add more homes to the city, the housing affordability crisis will only get worse.

[^1]: 6.75% is the rate of a 30 year mortgage as of 03/2023. Retrieved from: https://www.mortgagenewsdaily.com/mortgage-rates/mnd

[^2]: ["Gentle" density can save our neighborhoods](https://www.brookings.edu/research/gentle-density-can-save-our-neighborhoods/)
