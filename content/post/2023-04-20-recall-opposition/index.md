---
title: We Oppose the Recall Effort of Four Englewood Council Members
date: 2023-04-20T07:55:00-06:00
featured_image: ''
---

We at YIMBY Englewood oppose the current efforts to recall Members Nunnenkamp, Anderson, Wink, and Mayor Sierra. As was [reported by the Englewood Herald](https://englewoodherald.net/stories/englewood-resident-announces-effort-to-recall-mayor-city-council-members,430465), a group of residents have filed recall petitions with the city clerk for the members listed above.

The attempt to recall these council members mostly centers around their efforts to increase density in R-1 zoning, efforts that we support. These residents feel they have not been heard on zoning issues, but that is simply not the case. Numerous town halls and discussions with residents have been held. Anyone listening in on council meetings and study sessions knows they're trying to solve some very large important issues. It would be impossible to make everyone happy, but the solutions proposed are pragmatic and a helpful step in addressing housing attainability and affordability. Furthermore, the [council has decided to pause any adjustments to R-1 zoning in the city](https://englewoodherald.net/stories/englewood-halts-r-1-zoning-change-considerations,428400), considering the state land use bill may likely override anything the city passes.

The petitioners have also stated they're recalling the council members as they have "failed to address the rampant crime and drug problems that persist in the city." As Mayor Sierra pointed out during the council meeting, the council acts as a single body. There is no legitimate reason to single out these four members with respect to crime and drug problems.

This recall effort will be an absurd waste of taxpayer dollars. In 2018, [the city spent $15,000 on a _failed_ recall attempt of former council member Laurett Barrentine](https://www.9news.com/article/news/local/next/englewood-spending-thousands-on-an-unexpected-recall-election/73-586389635). This is is a distraction to the city council, and forces the city to spend precious taxpayer dollars on what is ultimately a farce.

If you are asked to sign a recall petition for these council members, we urge you to decline to sign it. Hopefully, this can be stopped before it even reaches the ballot box. Should this recall effort make it to a ballot, we _must_ vote to retain these members.
