---
title: Helping the Climate with Density
date: 2023-03-14
featured_image: 'images/cover.jpg'
---

The world is in crisis. Climate change affects our daily lives with extreme temperatures and weather. Scientists agree that the leading cause of climate change is greenhouse gas emissions, specifically carbon emissions. Did you know that drawing more people into our cities could help reduce our overall greenhouse gas emissions?

Take a look at [this article from The Guardian](https://www.theguardian.com/us-news/2021/aug/22/cities-climate-change-dense-sprawl-yimby-nimby). Here's some great quotes:

> Drawing more people into cities could help significantly shrink the country’s overall greenhouse gas emissions. Low-density developments produced nearly four times the greenhouse gas emissions of high-density alternatives, with research finding that doubling urban density can reduce carbon pollution from household travel by nearly half and residential energy use by more than a third.

Why is that? One reason may simply be that people in denser developments simply drive less.

> Research has found that people living in neighborhoods that are walkable, unsurprisingly, drive a quarter less than those in more spread out areas.

Climate emissions are not the only thing that density helps with. [According to the EPA](https://www.epa.gov/smartgrowth/protecting-water-resources-higher-density-development), density helps protect our water resources! Anyone who has lived in Colorado for a decent amount of time knows just how precious water is for us. As my dad used to tell me, "Whiskey's for drinkin' and water's for fightin' over."

From the EPA:

> - Higher-density scenarios generate less stormwater runoff per house at all scales and at all time-series build-out examples.
> - For the same amount of development, higher-density development produces less runoff and less impervious cover than low-density development.
> - For a given amount of growth, lower-density development affects more of the watershed.
