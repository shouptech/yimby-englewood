---
title: Visions of a More Walkable City
date: 2023-03-15
featured_image: 'images/cover.jpg'
---

Walking has numerous benefits. It's a good form of exercise and you can go places without burning fossil fuels. Did you know that dense housing may lead to more people walking? According to [this article by the University of Washington's School of Public Health](https://sph.washington.edu/news-events/news/study-shows-dense-housing-may-lead-more-people-walking), that may very well be the case!

> The research, published online in April in the journal Health & Place, found that housing density was the strongest predictor of walking frequency among Seattle-area residents across both high- and low-income neighborhoods.

Great news! While this study happened in Seattle, we can easily see the same benefits in Englewood. In fact, we see more people walking already in the densest parts of our city. Just sit on a bench in Downtown Englewood and count the number of people walking. This is also, coincidentally, the area of our city zoned with the highest density.
