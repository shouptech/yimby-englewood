---
title: Missing Middle Housing
date: 2023-04-01T12:00:00-06:00
featured_image: 'images/middle.jpg'
---

Housing affordability is the overarching goal of most YIMBY movements.  This is because as wages have stagnated and housing become short most places to live have become unaffordable.  In Colorado, unless you were particularly lucky you probably haven’t been able to find an affordable house in the last 10 years.  Why this is is complex and multifaceted but part of the problem is the lack of [missing middle housing](https://en.wikipedia.org/wiki/Missing_middle_housing).  The types of homes that are now missing (the middle homes between apartments and single family) cannot be built in single family neighborhoods.  This type of exclusionary practice has put a high price on the land under each single family home.  Also this practice of only building single family homes in areas is new; before WWII it was often the practice to see single family houses next to a row home.  If you’ve ever done any walking in older parts of Englewood, Denver, Golden, Greeley, or Ft. Collins you will see a mix of a lot of different types of housing.  The method to fix housing is to fix the supply shortage, and to fix the supply shortage you need to build more housing.  Englewood faces a large obstacle for building more housing, it is landlocked.  Englewood cannot grow out any more, therefore we have to infill in order to generate more housing stock.

Building more housing stock is a boon for many reasons; it allows people to get out of apartments and into home ownership, it will build diversity into neighborhoods, raise the property value of the homes around the new stock, and provide a variety of housing to people at various income levels.  An [article in Forbes](https://www.forbes.com/sites/jennifercastenson/2021/02/17/missing-middle-housing-is-a-huge-opportunity-offering-resilient-investment-and-high-demand/?sh=66a56cbd2e1c) describes how adding middle housing to the Houston and Dallas markets would be a boon for both cities.  As people move for work or university it is essential that we have enough apartments for newcomers and people who enjoy a less permanent living situation, as well as, a variety of different homes from small row homes to single family so that everyone has a place to live at an affordable price.

***

_Cover image is from the Sightline Institute Modest Middle Homes Library [on Flickr](https://www.flickr.com/photos/sightline_middle_housing/48376549317/in/photostream/)._
