---
title: "Help Out!"
menu:
  main:
    weight: 2
---

Does all of this sound great? Want to know how you can help?

## Make your voice heard!

There are a number of things you can do make your voice heard, and the most effective way is to let our representatives know your opinion!

### Talk to the city

- [Provide Feedback on the CodeNext Website](https://www.engaged.englewoodco.gov/codenext) - The city has built a platform for communications regarding this and other projects. If you have questions are want to lead feedback, you're encouraged to do so!
- [Contact the city council](https://www.englewoodco.gov/government/city-council) - Our city is governed by a wonderful council of residents. They love to hear from residents. Please contact your city council member!
- [Attend a city council meetings](https://englewoodgov.civicweb.net/portal/) - City council meetings are where decisions are made, and voices are heard! You can contact the city clerk to sign up for a time to address the council during a meeting. You can attend meetings both in person and virtually via Zoom! If you have time and are able to, please attend these meetings.

### Contact your representatives

Contact our state representatives! Our state is also looking at making changes in zoning and how to build in Colorado.  Let them know your opinions.

- State representative: Representative Meg Froelich meg.froelich.house@coleg.gov
- State senator: Senator Jeff Bridges jeff.bridges.senate@coleg.gov

### Contact RTD

We have two light rail stations in Englewood and several major thoroughfares. Incremental growth also means incremental changes to our transit access. Contact our RTD representative and let them know what you would love to see in our transit system.

- RTD District D representative:  Bobby Dishell:  Bobby.Dishell@rtd-denver.com
